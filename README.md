# Training Models on Imbalanced Text Data

**Topics** include Text Classification, Oversampling, Text Generation, RNN

https://www.manning.com/liveproject/training-models-on-imbalanced-text-data

Projet gitlab avec les notebooks

https://gitlab.com/garaud/nlp-liveproject-imbalanced-text-data
